const HAS_ERROR        = 'has-error';
const IS_SUBMITED      = 'is-submited';
const DATA_SUCCESS_MSG = 'successMsg';
const DATA_RULES       = 'rules';

export {
  HAS_ERROR,
  IS_SUBMITED,
  DATA_SUCCESS_MSG,
  DATA_RULES
}