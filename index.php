<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>starter</title>
  <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/scripts/get_bundle_name.php') ?>

  <link rel="stylesheet" href="<?php echo get_bundle_name()['css'] ?>">
</head>
<body>
  <div class="o-site">
    <div class="o-flex">
      <h1 class="t1 u-mt u-midnight-color">starter</h1>
      <div class="c-form js-validate-scope u-mb">
        <div class="o-container">
          <div class="js-item"></div>
          <div class="js-item"></div>
          <div class="js-item"></div>
          <div class="js-item"></div>
          <div class="js-item"></div>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo get_bundle_name()['js'] ?>"></script>
</body>
</html>